const express = require('express');
const { Actor, webfinger, stringify } = require('node-activitypub');
const getenv = require('getenv');
const bodyParser = require('body-parser');
const basicAuth = require('express-basic-auth');
const html = require('escape-html-template-tag');
const sanitize = require('sanitize-html');
sanitize.defaults.allowedAttributes['*'] = ['class', 'href', 'alt'];
sanitize.defaults.allowedTags.concat([ 'img' ]);

const home = require('./home')

const dbCollection = name => ({
	async add(f) {
		const items = await this.get();
		items.push(f);
		require('fs').writeFileSync('./json/' + name + '.json', JSON.stringify(items, null, 2));
	},
	async get() {
		try {
			return require('./json/' + name + '.json');
		} catch(e) {
			return [];
		}
	}
});

const followers = dbCollection('follower');
const notes = dbCollection('notes');
const feed = dbCollection('feed');

const port = getenv('PORT', 3000);
const host = getenv('HOST', 'http://localhost:' + port);
const keypair = getenv('KEYPAIR', '');
const password = getenv('PASSWORD', 'password');

const path = url => (new URL(url)).pathname;


const inbox = [];
const outbox = [];
const logger = {
	log(...args) {
		if(args.length > 1) {
			outbox.push(args);
		} else {
			outbox.push(args[0]);
		}
	},
	warn(...args){
		this.log(...args)
	}
}

const username = 'news';
Actor.paths.outbox = 	user =>  Actor.paths.id(user) + '/outbox';

const paul = new Actor({
	host,
	username,
	name: 'Node-Activitypub Updates',
	summary: `<p>I'm working on a Node JS ActivityPub library. Follow me for updates!</p>`,
	url: host,
	attachment: [
		{
			type: 'PropertyValue',
			name: 'Also at',
			value: "<a href=\"https://kith.kitchen/@paul\" rel=\"me nofollow noopener noreferrer\" target=\"_blank\"><span class=\"invisible\">https://</span><span class=\"\">kith.kitchen/@paul</span><span class=\"invisible\"></span></a>"
		}
	],
	icon: {
		type: 'Image',
		mediaType: 'image/png',
		url: host + '/fedi-js.png'
	}
},
keypair ? JSON.parse(keypair) : void 0,
{
	console: logger
});

function queue(fn) {
	setTimeout(fn, 1000);
}

const app = express();

app.use(express.static('static'));

app.use(webfinger(u => u===username ? paul.id : null));

app.get(paul.paths.id, (req, res) => {
	res.setHeader('content-type', 'application/activity+json');
	res.send(stringify(paul));
});

app.use(bodyParser.json({ type: 'application/activity+json' }));
app.use(bodyParser.urlencoded());

app.post(paul.paths.inbox, async (req, res, next) => {
	res.setHeader('content-type', 'application/activity+json');
	try {
		const actor = await paul.getClient().verifySignature(req);

		switch(req.body.type) {
			case 'Follow':
				if((req.body.actor.id || req.body.actor) === actor.id) {
					followers.add({ id: actor.id, inbox: actor.inbox });
					queue(() => paul.accept(req.body, actor.inbox));
				}
				break;
			case 'Create':
				if(req.body.actor === req.body.object.attributedTo) {
					feed.add(req.body.object);
				}
				break;
			case 'Announce':
			case 'Like':
				feed.add(req.body);
				break;
			default:
				inbox.push(req.body);
		}
		res.sendStatus(201);
	} catch(e) {
		next(e);
	}
});

app.get(path(paul.followers), async (req, res) => {
	const f = await followers.get();
	res.setHeader('content-type', 'application/activity+json');
	res.send(stringify(
	{
		"@context": "https://www.w3.org/ns/activitystreams",
		id: paul.followers,
		type: "Collection",
		totalItems: f.length,
		items: f.map(f => f.id)
	}));
});

app.get(path(paul.outbox), async (req, res) => {
	res.setHeader('content-type', 'application/activity+json');
	res.send(stringify(
	{
		"@context": "https://www.w3.org/ns/activitystreams",
		id: paul.outbox,
		type: "OrderedCollection",
		totalItems: 0,
		orderedItems: []
	}));
});


app.get(path(paul.following), async (req, res) => {
	res.setHeader('content-type', 'application/activity+json');
	res.send(stringify(
	{
		"@context": "https://www.w3.org/ns/activitystreams",
		id: paul.following,
		type: "Collection",
		totalItems: 0,
		items: []
	}));
});

app.get('/notes/:ix', async (req, res, next) => {
	if(req.params.ix < (await notes.get()).length) {
		res.setHeader('content-type', 'application/activity+json');
		res.send(stringify((await notes.get())[req.params.ix]));
	} else {
		next();
	}
})

app.get('/', async (req, res) => {
	res.send(home(html`Follow <a href="${paul.id}">@${username}@${host.split('//')[1]}</a> for updates.
	<hr>
	${(await notes.get()).map(n => html`<div>${html.safe(n.content)}</div>`)}`));
});

const admin = new express.Router();

app.use('/admin', admin);

admin.use(basicAuth({
	users: { admin: password },
	challenge: true,
	realm: 'activitypub'
}));

const actors = {}

const getActor = async uri => {
	if(actors[uri]) {
		return actors[uri];
	} else {
		const actor = await paul.getClient().get(uri);
		actors[uri] = actor;
		return actor;
	}
}

admin.get('/', async (req, res) => {
	res.send(html`
<!doctype html>
<html>
<body>
<form method="post" action="/admin/post">
<textarea name="message"></textarea>
<button>Post</button>
</form>
Followers:
<pre>
${JSON.stringify(await followers.get(), null, 2)}
</pre>

Feed:
${await Promise.all((await feed.get()).map(
	async item => {
		try {
			switch(item.type) {
			case 'Note':
				var actor = await getActor(item.attributedTo);
				return html`<div style="display:flex;border: 1px solid black;">
					<div>${actor && actor.name} (<a href="${actor && actor.id}">${actor && actor.preferredUsername}</a>)<br>
					<img src="${actor && actor.icon && actor.icon.url}" width=100></div>
					<div style="margin-left: 20px;">
						${html.safe(sanitize(item.content))}
						<details>
							<summary>Original message</summary>
							<pre>
							${JSON.stringify(item, null, 2)}
							</pre>
						</details>
					</div>
				</div>`;
			case 'Like':
			case 'Announce':
				var actor = await getActor(item.actor);
				return html`<div style="display:flex;border: 1px solid black;">
					<div>${actor && actor.name} (<a href="${actor && actor.id}">${actor && actor.preferredUsername}</a>)<br>
					<img src="${actor && actor.icon && actor.icon.url}" width=100></div>
					<div style="margin-left: 20px;">
						${actor && actor.name} did a ${item.type}
						<details>
							<summary>Original message</summary>
							<pre>
							${JSON.stringify(item, null, 2)}
							</pre>
						</details>
					</div>
				</div>`;
			default:
				return html`<pre>${JSON.stringify(item, null, 2)}</pre>`
			}
		} catch (e) {
			return html`<pre>${e}</pre><pre>${JSON.stringify(item, null, 2)}</pre>`;
		}
	}
))}

Inbox:
<pre>
${JSON.stringify(inbox, null, 2)}
</pre>

Logs:
<pre>
${html.join(outbox, '\n')}
</pre>
	`.toString())
})

admin.post('/post', async (req, res) => {
	let message = req.body.message;
	const tags = [];
	const cc = [paul.followers];

	const mentions = Array.from(message.matchAll(/[a-z0-9._+=~]+@([a-z0-9._+=~])(?::[0-9]+)+/g)).reverse();
	for(const mention of mentions) {
		try {
			const actorUrl = await paul.getClient().resolveWebfinger(mention[0], mention[1] === 'localhost' ? 'http' : 'https');
			message = message.slice(0, mention.index)
				+ `<a href="${actorUrl.replace(/"/g, "&quot;")}" rel="nofollow">${mention[0]}</a>`
				+ message.slice(mention.index + mention[0].length)
			tags.push({
				type: "Mention",
				href: actorUrl,
				name: mention[0]
			});
			cc.push(actorUrl);
		} catch(e) {
			console.log(e);
			continue;
		}
	}

	const nodeId = host + '/notes/' + (await notes.get()).length;
	const note = {
		id: nodeId,
		type: 'Note',
		published: (new Date).toISOString(),
		url: nodeId,
		attributedTo: paul.id,
		to: ["https://www.w3.org/ns/activitystreams#Public"],
		cc,
		content: message,
		tag: tags
	};

	await notes.add(note);

	const create = paul.activity('Create', note);

	const inboxes = Object.fromEntries((await followers.get()).map(u => [u.id, u.inbox]));

	for(const id of cc.slice(1)){
		if(!(id in inboxes)) {
			inboxes[id] = (await (paul.getClient().get(id))).inbox;
		}
	}

	for(const inbox of Object.values(inboxes)) {
		queue(() => paul.getClient().post(inbox, create));
	}

	res.redirect('/admin');
});


app.listen(port);
