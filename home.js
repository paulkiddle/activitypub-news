const html = require('escape-html-template-tag');

const dependency = require('./package.json').dependencies['node-activitypub'];

module.exports = (posts) => html`<!DOCTYPE html>
<html>
<head>
<title>Node-ActivityPub</title>
<style>
body {
	display: grid;grid-template-columns: 1fr 1fr;grid-auto-flow: dense;
}

section {
	grid-column: 1;
}

</style>
</head>
<body>
	<section style="grid-column: 1 / 3;">
		<h1>Node-ActivityPub</h1>

		<p>
			Node-ActivityPub is a low-level set of tools to help you communicate with the fediverse.
			The library is designed to be unobtrusive and unopinionated, designed to keep you on spec and not much more.
		</p>

		<p>
			It's designed to work with node's built in http server, so you don't need express if you don't want it-- although it still works nicely with express.
		</P>
	</section>

	<section>
		<h2>Contents</h2>

		<ul>
			<li>Helpers for ingoing/outgoing messages - for example to serialize objects and verify json-ld signatures</li>
			<li>Actor/Client class for signing and sending server-to-server ActivityPub messages</li>
			<li>A helper for handling webfinger requests</li>
		</ul>
	</section>

	<section>
		<h2>Quick start</h2>
		<pre>
npm install ${dependency}
		</pre>

		<pre>
const me = new Actor({
	username: 'Paul',
	host: 'https://example.com'
});

const followers = [];

app.get(me.paths.id, (req, res) => {
	send(res, me);
});

app.post(me.paths.inbox, async (req, res) => {
	const actor = await verifySignature(req);
	const activity = req.body;

	switch(activity.type) {
		case 'Follow':
			followers.push(actor);
			me.accept(activity);
	}
});

const note = {
	type: 'Note',
	content: 'Hello world!',
	to: AS_PUBLIC,
	cc: me.followers,
	attributedTo: me.id
}
const create = me.activity('Create', note);

for(const follower of followers) {
	me.post(follower.inbox, create);
}
		</pre>
	</section>
	<section style="grid-column: 2;grid-row: 2/4;">
		<h2>News</h2>
		${posts}
	</section>
</body>
`.toString();
